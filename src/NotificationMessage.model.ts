export class NotificationMessage {
  constructor(
    public id: string,
    public header: string,
    public body: string,
    public messageType: string,
    public seen: string = 'false'
  ) {}
}
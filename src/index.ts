import { NgModule, ModuleWithProviders } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PingComponent } from './ping.component';
import { PingService } from './ping.service';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations'

export * from './ping.component';
export * from './ping.service';
export * from './NotificationMessage.model';
export * from '@angular/platform-browser/animations'

@NgModule({
  imports: [
    CommonModule,
    BrowserAnimationsModule
  ],
  declarations: [
    PingComponent,
  ],
  exports: [
    PingComponent,
  ]
})
export class PingModule {
  static forRoot(): ModuleWithProviders {
    return {
      ngModule: PingModule,
      providers: [PingService,]
    };
  }
}

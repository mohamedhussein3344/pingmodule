import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {PingComponent} from "./ping.component";
import {DebugElement} from '@angular/core';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/observable/of';
import {By} from '@angular/platform-browser';
import {PingService} from "./ping.service";
import {close} from "fs";

describe('PingComponent', () => {
    let comp: PingComponent;
    let fixture: ComponentFixture<PingComponent>;

    let spy: jasmine.Spy;
    let de: DebugElement;
    let el: HTMLElement;
    let pingService: PingService;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [ PingComponent ],
            providers: [ PingService ]
        });
        fixture = TestBed.createComponent(PingComponent);        
        comp = fixture.componentInstance;
        comp.displayedNotifications = [{id: '1' ,header: 'test header', body: 'test content', messageType: 'info', seen: 'false'},{id: '2' ,header: 'test header', body: 'test content', messageType: 'info', seen: 'false'},{id: '3' ,header: 'test header', body: 'test content', messageType: 'info', seen: 'false'}];
        comp.notificationArr = [];
        //pingService = fixture.debugElement.injector.get(PingService);
        //spy = spyOn(pingService, 'addNotification').and.returnValue(Observable.of({}));
    }));

    it('should be created', () => {
        expect(comp).toBeTruthy();
    });

    it('should inject the servuce', ()=>{

    });

    //display function unit test
    it( " shouldn't display anything new ", ()=> {
       expect(comp.display()).toBeFalsy();
    });

    it( " should display new notification " , ()=> {
       comp.notificationArr = [{id: '5' ,header: 'test header', body: 'test content', messageType: 'info', seen: 'false'}
                                ,{id: '4' ,header: 'test header', body: 'test content', messageType: 'info', seen: 'false'},
                                {id: '6' ,header: 'test header', body: 'test content', messageType: 'info', seen: 'false'}];
       comp.display();
       var newNotification = comp.displayedNotifications.find((el)=> {
           return el.id == '5';
       })
       expect(newNotification).toBeDefined();
       var removedNotification = comp.notificationArr.find((el)=> {
           console.log('the el is:' + JSON.stringify(el));
           return el.id == '5';

       })
        expect(removedNotification).toBeUndefined();
    });

    it('should remove notifcation after (n) time', ()=> {
        spyOn(window , 'setTimeout').and.callFake((fn)=> {
            fn();
        });

        var data = [{id: '5' ,header: 'test header', body: 'test content', messageType: 'info', seen: 'false'},{id: '4' ,header: 'test header', body: 'test content', messageType: 'error', seen: 'false'},{id: '6' ,header: 'test header', body: 'test content', messageType: 'info', seen: 'false'}];
        comp.notificationArr =  data;
        spyOn( comp, 'close');
        comp.display();
        expect(comp.close).toHaveBeenCalledWith('5');

    })

    //close function unit test
    it('should remove notifiction from array', ()=>{
        var arrLength = comp.displayedNotifications.length;
        comp.close('1');
        expect(comp.displayedNotifications.length).toBe(arrLength - 1);
        var x = comp.displayedNotifications.find((el)=>{
            return el.id == '1';
        })
        expect(x).toBeUndefined();
    })

    it(' should call display function', ()=>{
        spyOn(comp, 'display');
        comp.close('1');
        expect(comp.display).toHaveBeenCalled();
    })

});

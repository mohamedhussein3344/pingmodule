import { Component, OnInit } from '@angular/core';
import { trigger , state, style, transition, animate } from '@angular/animations';
import { NotificationMessage } from './NotificationMessage.model';
import { PingService } from './ping.service';
import * as _ from 'lodash'

const slideInOutAnimation =
    trigger('divState', [

        // end state styles for route container (host)
        state('*', style({
            // the view covers the whole screen with a semi tranparent background
            'height': '64px',
            'border-radius': '2px',
            'margin-bottom': '5px',
            'color': '#000',
            'box-shadow': '0 2px 10px rgba(0,0,0,0.2)',
            'position': 'relative',
            //'transform': 'translateX(320px)'
        })),

        // route 'enter' transition
        transition(':enter', [

            // styles at start of transition
            style({
                // start with the content positioned off the right of the screen,
                // -400% is required instead of -100% because the negative position adds to the width of the element
                'transform': 'translateX(320px)',

                // start with background opacity set to 0 (invisible)
                //backgroundColor: 'rgba(0, 0, 0, 0)'
            }),

            // animation and styles at end of transition
            animate('.5s ease-in-out', style({
                // transition the right position to 0 which slides the content into view
                //right: 0,
                'transform': 'translateX(0px)',
                // transition the background opacity to 0.8 to fade it in
                //backgroundColor: 'rgba(0, 0, 0, 0.8)'
            }))
        ]),

        // route 'leave' transition
        transition(':leave', [
            // animation and styles at end of transition
            animate('.2s ease-in-out', style({
                // transition the right position to -400% which slides the content out of view
                'transform': 'translateX(320px)',

                // transition the background opacity to 0 to fade it out
                //backgroundColor: 'rgba(0, 0, 0, 0)'
            }))
        ])
    ]);

@Component({
  selector: 'ping-component',
  template: `
      <div class="notifications">
        <div *ngFor=" let notificationMessage of displayedNotifications"
          class="notifications__box {{notificationMessage.messageType}}" [@divState]=" notificationMessage.seen ? ':enter' : ':leave' ">

          <div class="close" (click)='close(notificationMessage.id)'><i class="fa fa-window-close"></i></div>
          <div class="row">

            <div class="col-md-2">
              <i class="fa fa-{{notificationMessage.messageType}}"></i>
            </div>

            <div class="col-md-10">
              <h2>{{ notificationMessage.header }}</h2>
              <p>{{ notificationMessage.body }}</p>
            </div>

          </div>
        </div>
    </div>
  `,
  styleUrls: [
    'ping.component.scss'
  ],
  animations:[ slideInOutAnimation
        // trigger('divState', [
        //     state('true', style({
        //             transform: 'translateX(0)',
        //         }
        //     )),
        //     state('false', style({
        //         transform: 'translateX(320px)'
        //     })),
        //     transition('false => true', animate(300))
        // ]),

  ]

})
export class PingComponent implements OnInit {

  notificationArr: NotificationMessage[];
  displayedNotifications = [];
  //constructor
  constructor(private notificationsService: PingService) {
  }
  //ngOnInit 
  ngOnInit() {
    this.notificationArr = this.notificationsService.notificationArr;

    for(let i=4; i>=0; i--) {
      //this.notificationArr[i].seen = true;
      this.display();
    }

    this.notificationsService.notifications.subscribe((messages) => {
      //console.log('emitted!');
      this.notificationArr = messages;
      this.display();
    });
  }
  display() {

    if(this.notificationArr.length == 0 || this.displayedNotifications.length == 5) {
          return false;
    }

    let message = this.notificationArr.shift();
    message.seen = 'true';
    this.displayedNotifications.push(message);
    localStorage.setItem('notificationArrStorage', JSON.stringify(this.notificationArr));

    if(message['messageType'] == 'info') {
        setTimeout(() => {
            this.close(message['id']);
        },10000);
    }

  }
  close(id) {
    _.remove(this.displayedNotifications, (o) => {
      return o['id'] == id;
    });
    this.display();
  }

}

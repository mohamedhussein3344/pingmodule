import { TestBed, inject } from '@angular/core/testing';
import { PingService  } from './ping.service';
//import { PingComponent } from './ping.component';
//import { FormsModule } from '@angular/forms';

describe('PingService', () => {

    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [],
            providers: [PingService]
        });
    });

    it('should be created', inject([PingService], (service: PingService) => {
        expect(service).toBeTruthy();
    }));

    it('should add id to the object', inject([PingService], (service: PingService)=> {
        let data = {id:'', header:'test',body: 'test', messageType: 'test' };
        let arrayLength = service.notificationArr.length;
        spyOn(service, 'create_UUID').and.callThrough();
        service.addNotification({data});
        expect(service.create_UUID).toHaveBeenCalled();
        expect(service.notificationArr.length).toBe( arrayLength + 1);
        expect(service.notificationArr['id']).not.toBe(null);
        expect(service.notificationArr[0]['id']).toBeDefined();
    }));

    it('should add notification', ()=> {

    })



});
import { Injectable, EventEmitter } from '@angular/core';
import { NotificationMessage } from './NotificationMessage.model';

@Injectable()
export class PingService {
  notificationArr: NotificationMessage[] = [];
  constructor() { 
    this.notificationArr = JSON.parse(localStorage.getItem('notificationArrStorage')) || [];
  }
  notifications = new EventEmitter<any>();
  //create super unique id
  create_UUID() {
    let dt = new Date().getTime();
    const uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
      const r = (dt + Math.random() * 16) % 16 | 0;
      dt = Math.floor(dt / 16);
      return (c == 'x' ? r : (r & 0x3 | 0x8)).toString(16);
    });
    return uuid;
  }
  //get data from Notifcation Source
  addNotification(msg){
     msg['id'] = this.create_UUID();
     msg['seen'] = false;
     this.notificationArr.push(msg);
     this.notifications.emit(this.notificationArr);
  }
}